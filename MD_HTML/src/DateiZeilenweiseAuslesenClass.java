import java.io.BufferedReader; 
import java.io.File; 
import java.io.FileReader; 
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays; 

public class DateiZeilenweiseAuslesenClass { 
	static ArrayList<String> text = new ArrayList<String>();

	private static void ladeDatei(String datName) { 

		File file = new File(datName); 

		if (!file.canRead() || !file.isFile()) 
			System.exit(0); 

		BufferedReader in = null; 
		try { 
			in = new BufferedReader(new FileReader(datName)); 
			String zeile = null; 
			while ((zeile = in.readLine()) != null) { 
				System.out.println(zeile); 
				text.add(zeile); 

			} 
		} catch (IOException e) { 
			e.printStackTrace(); 
		} finally { 
			if (in != null) 
				try { 
					in.close(); 
				} catch (IOException e) { 
				} 
		} 
	} 

	public static void replace(){

		int x=0;
		for(int i =0; i < text.size();i++){

			if(text.get(i).contains("###### ")){
				String text0=text.get(i).replaceAll("###### ", "<h3>");
				System.out.println(text0 + "</h3>");
			}
			else if(text.get(i).contains("## ")){
				String text0=text.get(i).replaceAll("## ", "<h2>");
				System.out.println(text0 + "</h2>");
			}
			else if(text.get(i).contains("# ")){
				String text0=text.get(i).replaceAll("# ", "<h1>");
				System.out.println(text0 + "</h1>");
			}
			else if(text.get(i).contains("* ")){
				x++;
				if(x==1)System.out.print("<ul>");
				String text0=text.get(i).replace("* ", "<li>");
				System.out.print(text0 + "</li>");
				if(text.get(i+1).contains("* ")==false){
					System.out.print("</ul>");
					x=0;
				}
			}
			else{
				System.out.println(text.get(i));
			}
			if(text.get(i).contains("_")){
				String text0=text.get(i).replaceAll("_", "<i>");
				System.out.println(text0 + "</i>");
			}
			if(text.get(i).contains(" *")){
				String text0=text.get(i).replaceAll(" *", "<i>");
				System.out.println(text0 + "</i>");
			}
			if(text.get(i).contains("__")){
				String text0=text.get(i).replaceAll("__", "<b>");
				System.out.println(text0 + "</b>");
			}
			if(text.get(i).contains("**")){
				String text0=text.get(i).replaceAll("**", "<b>");
				System.out.println(text0 + "</b>");
			}
			if(text.get(i).contains("***")){
				String text0=text.get(i).replaceAll("***", "<i><b>");
				System.out.println(text0 + "</i></b>");
			}
			if(text.get(i).contains("___")){
				String text0=text.get(i).replaceAll("___", "<i><b>");
				System.out.println(text0 + "</i></b>");
			}



			System.out.println();
		}

		//ol li


	}


public static void main(String[] args) { 
	String dateiName = "/Users/florianbartenbach/Desktop/test.md"; 
	ladeDatei(dateiName); 
	System.out.println("_____________________________________");

	replace();

} 
}